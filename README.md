# Project 6: Brevet time calculator service

Simple listing service from project 5 stored in MongoDB database.

## Author: Taylor C. Brennan, tbrennan@uoregon.edu ##
 

## Recap 

# ACP controle times

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders). 
As noticed from the calculator, 
-any control past the brevet distance is equal to the brevent distance.
-can only have controls up to 120% of brevet (ie. a 200km brevet can have a max of 240KM) Validation for this is handled though app.py
-can only have floats.  Validation for this is handled though app.py
-closing times before 15KM will add an hr (count for open time) like the calculator, it it not suggested to have controls so close to the start
-in both open and close, time are calculated based on the distance range. (0-200) (200-400) etc. So for 400KM, the first 200kM will be calculated by the rules of 0-200 and the second 200km will be calculated with the rules of 200-400.

Open times are calulated by [(200 , 34), (400, 32), (600, 30), (1000, 28), (1300, 26)]
-The first 200km are calculated if you were going 34 km/hr, 200-400km are calculated if you were going 32km/hr. So the total for the 400km point is 200km at 34km/hr + 200km at 32km/hr
-There is a weird case with 200km, where it will add 10min to fix the time.

# Submitting and Displaying

-If there is an illegal character, then the distance is set to 0 
-When submitting, if the previous value is larger then the current value, the current value will be skipped.


# AJAX and Flask reimplementation

The RUSA controle time calculator is a Perl script that takes an HTML form and emits a text page in the above link. 

The implementation that you will do will fill in times as the input fields are filled using Ajax and Flask. Currently the miles to km (and vice versa) is implemented with Ajax. You'll extend that functionality as follows:

* Each time a distance is filled in, the corresponding open and close times should be filled in with Ajax.   

* If out of range or bad characted are used, the open and close field will be blank, and there will be an error message at the bottom of the table


## Add(only working) Functionality

This project has following four parts. Change the values for host and port according to your machine, and use the web browser to check the results.

* the RESTful service to expose what is stored in MongoDB. 
    * "http://<host:port>/listAll"  returns all open and close times in the database
    * "http://<host:port>/listOpenOnly" should return open times only
    * "http://<host:port>/listCloseOnly" should return close times only

* You will also design two different representations: one in csv and one in json. For the above, JSON should be your default representation for the above three basic APIs. 
    * "http://<host:port>/listAll/csv" should return all open and close times in CSV format \
    * "http://<host:port>/listOpenOnly/csv" should return open times only in CSV format      \
    * "http://<host:port>/listCloseOnly/csv" should return close times only in CSV format    / I got stuck on here

    * "http://<host:port>/listAll/json" should return all open and close times in JSON format
    * "http://<host:port>/listOpenOnly/json" should return open times only in JSON format
    * "http://<host:port>/listCloseOnly/json" should return close times only in JSON format

* You will also add a query parameter to get top "k" open and close times. For examples, see below.

    * "http://<host:port>/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format 
    * "http://<host:port>/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
    * "http://<host:port>/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
    * "http://<host:port>/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format

* For the client portion. There is a basic table function that was based of the example https://css-tricks.com/dynamic-dropdowns/
The current working funtions are Option1 "View" Option2 (from view) "/listAll", "/listOpenTime", "/listCloseTime"
-The begining option menu is loaded, and when a slection is made, it requests the json of the options, and then gives the second option choices
-You can enter in a value in the open field and that will be passed the amount of result you would like
-I originaly got stuck for a while because I failed to install Docker correctly the first time, I was not able to finish the other options, which would have 
    similar to api methods. View=get, add=post, remove=delete. Those begin option value are commented out.

-The values entered into the database will not be sorted in any manor. The reason is the previous project handle the sorting prior to submitting to the server
-There is a proble that I need to find to connect the previous project with this one. The issue is it isn't porting correctly. whatever values I enter in the docker file, doesn't affect how it is read. While looking at the running containers, it would say 0.0.0.0:5002->5002/tcp, but inspecting the build, it clearly states that it is active on 0.0.0.0:5000. So I will be working on fixing this for proj 7

-I was also unable to finish the formating for csv. 

-BUT the top values work, I have it set up in a way that I was going to use for other purposes (adding, deleting control proints)


## Tasks

You'll turn in your credentials.ini using which we will get the following:

* The working application with three parts.

* Dockerfile

* docker-compose.yml

## Grading Rubric

* If your code works as expected: 100 points. This includes:
    * Basic APIs work as expected.
    * Representations work as expected.
    * Query parameter-based APIs work as expected.
    * Consumer program works as expected. 

* For each non-working API, 5 points will be docked off. If none of them work,
  you'll get 35 points assuming
    * README is updated with your name and email ID.
    * The credentials.ini is submitted with the correct URL of your repo.
    * Dockerfile is present. 
    * Docker-compose.yml works/builds without any errors.

* If README is not updated, 5 points will be docked off. 

* If the Docker-compose.yml doesn't build or is missing, 15 points will be
  docked off. Same for Dockerfile as well.

* If credentials.ini is missing, 0 will be assigned.
