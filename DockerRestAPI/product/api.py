# Laptop Service

from flask import Flask, json, jsonify, request
import flask
import csv
from flask_cors import CORS
from flask_restful import Resource, Api
from pymongo import MongoClient
import logging

# Instantiate the app
app = Flask(__name__)
api = Api(app)
client = MongoClient("172.19.0.2", 27017)
db = client.tododb
db.tododb.remove({})
CORS(app)

#Set of points to be used        
db.tododb.insert({"miles":0, "km":0, 'loc':"0Km", "open_t":"Sun 1/1 8:00", "close_t":"Sun 1/1 9:10", "notes":"Tests"})
db.tododb.insert({"miles":62, "km":100, 'loc':"100Km", "open_t":"Sun 1/1 10:56", "close_t":"Sun 1/1 14:50", "notes":"Tests"})
db.tododb.insert({"miles":49, "km":80, 'loc':"80Km", "open_t":"Sun 1/1 10:21", "close_t":"Sun 1/1 13:30", "notes":"Tests"})


@app.route('/listOpenOnly')
@app.route('/listOpenOnly/<filetype>')
def Opentime(filetype="json"):
	amount = request.args.get("top", 20, type=int)
	app.logger.debug("Entering Open time" + str(amount))
	output = []
	for s in db.tododb.find()[:amount]:
		output.append({'open_t' : s['open_t']})
		app.logger.debug("I have: " + str(output))
		
	return jsonify(output)

@app.route('/listCloseOnly')
@app.route('/listCloseOnly/json')
def Closetime():
	amount = request.args.get("top", 20, type=int)
	app.logger.debug("Entering Close time" + str(amount))
	output = []
	for s in db.tododb.find()[:amount]:
		output.append({ 'close_t' : s['close_t']})
	app.logger.debug("I have: " + str(output))
	return jsonify(output)

@app.route('/listAll')
@app.route('/listAll/json')
def ListAll():
	amount = request.args.get("top", 20, type=int)
	app.logger.debug("Entering Close time" + str(amount))
	output = []
	for s in db.tododb.find()[:amount]:
		output.append({'open_t' : s['open_t'], 'close_t' : s['close_t']})
	app.logger.debug("I have: " + str(output))
	return jsonify(output)




@app.route("/options")
def options():
	option = {"GETS": ",/listOpenOnly,/listCloseOnly, /listAll"}
	return jsonify(option)

# Create routes
# Another way, without decorators
#api.add_resource(CloseTime, '/listCloseOnly')
#api.add_resource(Both, '/listAll')
# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
